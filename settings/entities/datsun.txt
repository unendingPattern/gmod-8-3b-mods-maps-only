
// These files are server side
// Changes to these files will only effect a server that you create


"datsun"
{

	// The classname of the entity you're going to spawn.

	"classname"			"prop_vehicle_jeep"
	
	
	// the type of entity - this is for the server limit stuff.. and if you don't set it
	// player will be able to spawn an unlimited amount of these on your server!
	// Valid values are "vehicle", "prop", "ragdoll" or "effect"
	
	"type"				"vehicle"
	
	// These are any keyvalues that you can assign to an entity normally
	"keyvalues"
	{
		"model"				"models/datsun.mdl"
		"vehiclescript"		"scripts/vehicles/datsun_test.txt"
		"solid"				"6"
	}
}