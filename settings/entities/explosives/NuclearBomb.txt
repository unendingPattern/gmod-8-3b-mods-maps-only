"Jeep"
{
	"classname"			"prop_physics_override"
	"type"				"prop"
	"keyvalues"
	{
	"spawnflags" "256"
	"physdamagescale" "0.1"
	"inertiaScale" "1.0"
	"fademindist" "-1"
	"fadescale" "1"
	"model" "models\props_phx\misc\flakshell_big.mdl"
	"skin" "0"
	"massScale" "0"
	"ExplodeDamage" "100"
	"ExplodeRadius" "10000"
	"minhealthdmg" "0"
	"health" "30"
	"rendercolor" "255 0 0"
	}
}