
// These files are server side
// Changes to these files will only effect a server that you create


"Jeep"
{

	// The classname of the entity you're going to spawn.

	"classname"			"prop_vehicle_prisoner_pod"
	
	
	// the type of entity - this is for the server limit stuff.. and if you don't set it
	// player will be able to spawn an unlimited amount of these on your server!
	// Valid values are "vehicle", "prop", "ragdoll" or "effect"
	
	"type"				"vehicle"
	
	// These are any keyvalues that you can assign to an entity normally
	"keyvalues"
	{
	"model" "models/vehicles/prisoner_pod_inner.mdl"
	"solid" "6"
	"fademindist" "-1"
	"fadescale" "1"
	"vehiclescript" "scripts/vehicles/prisoner_pod.txt"
	"actionScale" "1"
	"OnClose" "!self,Unlock,,0,-1"
	"PlayerOn" "!self,color,255 0 0,0,-1"
	"PlayerOff" "!self,color,0 255 0,0,-1"
	"rendercolor" "0 255 0"
	}
}